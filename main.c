#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void handle_signal(int signal);
void do_sleep(int seconds);

int main(int argc, char** argv) {
    struct sigaction sa;
    
    printf("Hello my pid is %d\n", getpid());
    fflush(stdout);
    
    sa.sa_handler = &handle_signal;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);

    if(sigaction(SIGINT, &sa, NULL) == -1) {
        perror("Error: cannot handle SIGINT");
    }
    if(sigaction(SIGTERM, &sa, NULL) == -1) {
        perror("Error: cannot handle SIGTERM");
    }
    if (sigaction(SIGUSR1, &sa, NULL) == -1) {
        perror("Error: cannot handle SIGUSR1");
    }
    if (sigaction(SIGUSR2, &sa, NULL) == -1) {
        perror("Error: cannot handle SIGUSR2");
    }
    
    for (;;) {
        pause();
    }
    return (EXIT_SUCCESS);
}

void handle_signal(int signal) {
    const char *signal_name;
    sigset_t pending;

    switch (signal) {
        case SIGINT:
            signal_name = "SIGINT";
            break;
        case SIGTERM:
            signal_name = "SIGTERM";
            break;
        case SIGUSR1:
            signal_name = "SIGUSR1";
            break;
        case SIGUSR2:
            signal_name = "SIGUSR2";
            break;
        default:
            fprintf(stderr, "Caught wrong signal: %d\n", signal);
            return;
    }

    printf("Caught %d %s\n", signal, signal_name);
    printf("Done handling %s\n", signal_name);
}
